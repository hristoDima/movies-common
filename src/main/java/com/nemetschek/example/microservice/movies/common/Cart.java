package com.nemetschek.example.microservice.movies.common;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Cart {
    private List<Item> items;

    public double getCartPrice() {
        return items.stream().map(Item::getPrice).reduce(0.0, Double::sum);
    }
}
